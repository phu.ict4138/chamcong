from PyQt5.uic import loadUi
import sys
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
import cv2
from PyQt5 import QtWidgets
from LoadCam import Worker1
from threading import Thread
import detector
import os
import train
from datetime import datetime

def findName(ID):
    file = open("dataMember.csv", mode="r", encoding="utf-8-sig")
    line = file.readline()
    while line != "":
        list_1 = line.split(",")
        if list_1[0] == ID:
            return list_1[1] 
        line = file.readline()
    return "unknown"

def checkStatus(hour):
    list = hour.split(":")
    a = int(list[0])
    if a < 7:
        return "Đúng giờ"
    else:
        return "Muộn"

class People():
    def __init__(self, ID, hour, day):
        self.ID = ID
        if self.ID != "unknown":
            self.name = findName(ID)
            self.hour = hour
            self.day = day
            self.status = checkStatus(hour)
        else:
            self.name = "unknown"
            self.hour = "unknown"
            self.day = "unknown"
            self.status = "unknown"

class MainApp(QMainWindow):
    def __init__(self):
        super(MainApp, self).__init__()
        loadUi("GUI/mainapp.ui", self)
        self.btnChamcong.clicked.connect(self.Chamcong)
        self.btnThemnhanvien.clicked.connect(self.Themnhanvien)
        self.btnThongke.clicked.connect(self.Thongke)
        self.btnExit.clicked.connect(self.Exit)
        self.frameChamcong.setVisible(False)
        self.frameThemnhanvien.setVisible(False)
        self.frameThongke.setVisible(False)

        self.Worker1 = Worker1()
        self.detector = detector.Detector()
        self.trainer = train.Train()

        self.Chamcong()

    def Chamcong(self):
        self.Worker1.stop()
        self.Worker1 = None
        self.Worker1 = Worker1()

        self.frameChamcong.setVisible(True)
        self.frameThemnhanvien.setVisible(False)
        self.frameThongke.setVisible(False)
        self.btnChamcong.setStyleSheet(btnStyleFalse)
        self.btnThongke.setStyleSheet(btnStyleFalse)
        self.btnThemnhanvien.setStyleSheet(btnStyleFalse)
        self.btnChamcong.setStyleSheet(btnStyleTrue)

        self.Worker1.start()
        self.Worker1.ImageUpdate.connect(self.loadCamChamcong)

        self. btnCheck_Chamcong.clicked.connect(lambda: self.cham_cong(self.Worker1.rgb))

    def loadCamChamcong(self, Image):
        self.loadCam_Chamcong.setPixmap(QPixmap.fromImage(Image))
        self.loadCam_Chamcong.setScaledContents(True)

    def cham_cong(self, im):
        thread = Thread(target=lambda: self.take_face(im))
        thread.start()

    def take_face(self, im):
        copy = im.copy()
        box, name = self.detector.detect(copy,face_only=True)
        if(box!= None and name != None):
            copy,_,_ = self.detector.get_face(copy,box)
            copy= cv2.resize(copy, (200, 200))
            FlippedImage = cv2.flip(copy, 1)
            ConvertToQtFormat = QImage(FlippedImage.data, FlippedImage.shape[1], FlippedImage.shape[0],
                                       QImage.Format_RGB888)
            Pic = ConvertToQtFormat.scaled(220, 220, Qt.KeepAspectRatio)
            pixmap = QPixmap(Pic)
            self.imgCheck_Chamcong.setPixmap(pixmap)
            self.nameCheck_Chamcong.setText(findName(name))
            now = datetime.now()
            dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
            self.dateCheck_Chamcong.setText(dt_string)
            hour = now.strftime("%H:%M:%S")
            day = now.strftime("%d-%m-%Y")
            string = name + "," + findName(name) + "," + hour + "," + day + ","+"\n"
            file_path = day + ".txt"
            file = open(file_path, mode="a+", encoding="utf-8-sig")
            file.write(string)

    def Themnhanvien(self):
        self.Worker1.stop()
        self.Worker1 = None
        self.Worker1 = Worker1()

        self.frameChamcong.setVisible(False)
        self.frameThemnhanvien.setVisible(True)
        self.frameThongke.setVisible(False)
        self.btnChamcong.setStyleSheet(btnStyleFalse)
        self.btnThongke.setStyleSheet(btnStyleFalse)
        self.btnThemnhanvien.setStyleSheet(btnStyleFalse)
        self.btnThemnhanvien.setStyleSheet(btnStyleTrue)

        self.chosen_image = None
        self.Worker1.start()
        self.Worker1.ImageUpdate.connect(self.loadCamThemnhanvien)
        self.btnCapture_Themnhanvien.clicked.connect(lambda: self.take_copy(self.Worker1.rgb))


    def take_copy(self, im):
        copy = im.copy()
        copy = cv2.resize(copy, (600, 360))
        FlippedImage = cv2.flip(copy, 1)
        ConvertToQtFormat = QImage(FlippedImage.data, FlippedImage.shape[1], FlippedImage.shape[0],
                                   QImage.Format_RGB888)
        Pic = ConvertToQtFormat.scaled(400, 520, Qt.KeepAspectRatio)
        self.chosen_image = Pic
        pixmap = QPixmap(Pic)
        self.imgCapture_Themnhanvien.setPixmap(pixmap)
        self.imgCapture_Themnhanvien.setScaledContents(True)
        self.btnAddMem_Themnhanvien.clicked.connect(self.upload)

    def upload(self):
        name = self.edtName_Themnhanvien.text()
        ID = self.edtID_Themnhanvien.text()
        CCCD = self.edtCCCD_Themnhanvien.text()
        phone = self.edtPhone_Themnhanvien.text()
        role = self.cbRole_Themnhanvien.currentText()
        if (ID != "" and name != "" and CCCD != "" and phone != "" and role != ""):
            file = open("dataMember.csv", mode="a+", encoding="utf-8-sig")
            string = ID + "," + name + "," + CCCD + "," + phone + "," + role + "\n"
            file.write(string)
            image = self.chosen_image
            os.makedirs('Faces/' + ID)
            image.save('Faces/' + ID + '/' + ID + '.png')
            self.trainer.train()

    def loadCamThemnhanvien(self, Image):
        self.loadCam_Themnhanvien.setPixmap(QPixmap.fromImage(Image))
        self.loadCam_Themnhanvien.setScaledContents(True)

    def Thongke(self):
        self.Worker1.stop()
        self.Worker1 = None
        self.Worker1 = Worker1()

        self.frameChamcong.setVisible(False)
        self.frameThemnhanvien.setVisible(False)
        self.frameThongke.setVisible(True)
        self.btnChamcong.setStyleSheet(btnStyleFalse)
        self.btnThongke.setStyleSheet(btnStyleFalse)
        self.btnThemnhanvien.setStyleSheet(btnStyleFalse)
        self.btnThongke.setStyleSheet(btnStyleTrue)
        self.btnExport_Thongke.clicked.connect(self.exportExcel)

        now = datetime.now()
        day = now.strftime("%d - %m - %Y")
        title = "Danh sách chấm công ngày " + day
        self.tableName_Thongke.setText(title)
        self.tableWidgetThongke.setColumnWidth(0, 120)
        self.tableWidgetThongke.setColumnWidth(1, 300)
        self.tableWidgetThongke.setColumnWidth(2, 200)
        self.tableWidgetThongke.setColumnWidth(3, 200)
        self.tableWidgetThongke.setColumnWidth(4, 151)
        self.loadData()

    def exportExcel(self):
        now = datetime.now()
        day = now.strftime("%d-%m-%Y")
        file_path = "out/" + day +".csv"
        file = open(file_path, mode= "a+")
        file = open(file_path, mode="w")
        file.write("")
        file = open(file_path, mode="r", encoding="utf-8-sig")
        line = file.readline()
        if line =="":
            file2 = open(file_path, mode="w", encoding="utf-8-sig")
            file2.write("ID,Họ và tên,Giờ,Ngày,Trạng thái\n")
        file_path2 = day + ".txt"
        file2 = open(file_path2, mode="r", encoding="utf-8-sig")
        line = file2.readline()
        while line != "" and line != "\n":
            list_2 = line.split(",")
            person = People(list_2[0], list_2[2], list_2[3])
            if person.name != "unknown":
                string = person.ID + "," + person.name + "," + person.hour +"," + person.day +"," + person.status +"\n"
                file3 = open(file_path, mode= "a+", encoding="utf-8-sig")
                file3.write(string)
            line = file2.readline()

    def loadData(self):
        now = datetime.now()
        day = now.strftime("%d-%m-%Y")
        file_path = day +".txt"
        file  = open(file_path, mode = "r", encoding="utf-8-sig")
        row = file.readline()
        list = []
        while (row != "" and row !="\n"):
            line = row.split(",")
            person = People(line[0], line[2], line[3])
            list.append(person)
            row = file.readline()
        self.tableWidgetThongke.setRowCount(len(list))
        i = 0
        for person in list:
            self.tableWidgetThongke.setItem(i, 0, QtWidgets.QTableWidgetItem(person.ID))
            self.tableWidgetThongke.setItem(i, 1, QtWidgets.QTableWidgetItem(person.name))
            self.tableWidgetThongke.setItem(i, 2, QtWidgets.QTableWidgetItem(person.hour))
            self.tableWidgetThongke.setItem(i, 3, QtWidgets.QTableWidgetItem(person.day))
            self.tableWidgetThongke.setItem(i, 4, QtWidgets.QTableWidgetItem(person.status))
            i = i +1

    def Exit(self):
        exit(0)

btnStyleTrue = "border: none; background-color:rgb(255, 255, 255)"
btnStyleFalse = "border: none; background-color:rgb(184, 220, 185)"


if __name__ == "__main__":
    app = QApplication(sys.argv)
    widget = QtWidgets.QStackedWidget()
    mainApp = MainApp()
    widget.addWidget(mainApp)
    widget.setFixedWidth(1200)
    widget.setFixedHeight(700)
    widget.show()
    try:
        sys.exit(app.exec_())
    except:
        print("Exiting")