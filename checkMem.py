from PyQt5.uic import loadUi
import sys
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
import cv2
from PyQt5 import QtWidgets
from LoadCam import Worker1
from threading import Thread
import detector
import os
import train
from datetime import datetime

def findName(ID):
    file = open("dataMember.csv", mode="r", encoding="utf-8-sig")
    line = file.readline()
    while line != "":
        list_1 = line.split(",")
        if list_1[0] == ID:
            return list_1[1] 
        line = file.readline()
    return "unknown"

def checkStatus(hour):
    list = hour.split(":")
    a = int(list[0])
    if a < 7:
        return "Đúng giờ"
    else:
        return "Muộn"

class People():
    def __init__(self, ID, hour, day):
        self.ID = ID
        if self.ID != "unknown":
            self.name = findName(ID)
            self.hour = hour
            self.day = day
            self.status = checkStatus(hour)
        else:
            self.name = "unknown"
            self.hour = "unknown"
            self.day = "unknown"
            self.status = "unknown"
        
class CheckPeople(QMainWindow):
    def __init__(self, detect=None):
        super(CheckPeople, self).__init__()
        loadUi("GUI/check2.ui", self)
        self.btnAdd.clicked.connect(self.switchToAdd)
        self.btnExit.clicked.connect(self.exitApp)
        self.btnKq.clicked.connect(self.switchToKetQua)

        self.Worker1 = Worker1()
        self.detector = detector.Detector()

        self.Worker1.start()
        self.Worker1.ImageUpdate.connect(self.ImageUpdateSlot)
        self.btnCheck.clicked.connect(lambda: self.cham_cong(self.Worker1.rgb))

    def cham_cong(self, im):
        thread = Thread(target=lambda: self.take_face(im))
        thread.start()


    def take_face(self, im):
        copy = im.copy()

        box, name = self.detector.detect(copy,face_only=True)

        if(box!= None and name != None):
            copy,_,_ = self.detector.get_face(copy,box)
            copy= cv2.resize(copy, (200, 200))
            FlippedImage = cv2.flip(copy, 1)
            ConvertToQtFormat = QImage(FlippedImage.data, FlippedImage.shape[1], FlippedImage.shape[0],
                                       QImage.Format_RGB888)
            Pic = ConvertToQtFormat.scaled(220, 220, Qt.KeepAspectRatio)
            pixmap = QPixmap(Pic)
            self.imgCheck.setPixmap(pixmap)
            self.nameCheck.setText(findName(name))
            now = datetime.now()
            dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
            self.dateCheck.setText(dt_string)
            hour = now.strftime("%H:%M:%S")
            day = now.strftime("%d-%m-%Y")
            string = name + "," + findName(name) + "," + hour + "," + day + ","+"\n"
            file_path = day + ".txt"
            file = open(file_path, mode="a+", encoding="utf-8-sig")
            file.write(string)

    def ImageUpdateSlot(self, Image):
        self.loadCam.setPixmap(QPixmap.fromImage(Image))
        self.loadCam.setScaledContents(True)

    def CancelFeed(self):
        self.Worker1.stop()

    def switchToAdd(self):
        self.Worker1.stop()
        addPeople = AddPeople()
        widget.addWidget(addPeople)
        widget.setCurrentIndex(widget.currentIndex()+1)
        
    def switchToKetQua(self):
        self.Worker1.stop()
        ketQua = KetQua()
        widget.addWidget(ketQua)
        widget.setCurrentIndex(widget.currentIndex() + 1)

    def exitApp(self):
        exit(0)

class AddPeople(QMainWindow):
    def __init__(self, trainer=None):
        super(AddPeople, self).__init__()
        loadUi("GUI/add2.ui", self)
        self.switchCheck.clicked.connect(self.switchToCheck)
        self.btnExit.clicked.connect(self.exitApp)

        self.chosen_image = None

        self.Worker1 = Worker1()

        self.Worker1.start()
        self.Worker1.ImageUpdate.connect(self.ImageUpdateSlot)
        self.btnCapture.clicked.connect(lambda: self.take_copy(self.Worker1.rgb))

        if (trainer==None):
            self.trainer = train.Train()
        else:
            self.trainer = trainer

    def upload(self):
        name = self.edtName.text()
        ID = self.edtID.text()
        CCCD = self.edtCCCD.text()
        phone = self.edtPhone.text()
        role = self.cbRole.currentText()
        if (ID != "" and name != "" and CCCD != "" and phone != "" and role != ""):
            file = open("dataMember.csv", mode="a+", encoding="utf-8-sig")
            string = ID + "," + name + "," + CCCD + "," + phone + "," + role + "\n"
            file.write(string)
            image = self.chosen_image
            os.makedirs('Faces/' + ID)
            image.save('Faces/' + ID + '/' + ID + '.png')
            self.trainer.train()

    def take_copy(self, im):
        copy = im.copy()
        copy = cv2.resize(copy, (600, 360))
        FlippedImage = cv2.flip(copy, 1)
        ConvertToQtFormat = QImage(FlippedImage.data, FlippedImage.shape[1], FlippedImage.shape[0],
                                   QImage.Format_RGB888)
        Pic = ConvertToQtFormat.scaled(400, 520, Qt.KeepAspectRatio)
        self.chosen_image = Pic
        pixmap = QPixmap(Pic)
        self.imgCapture.setPixmap(pixmap)
        self.imgCapture.setScaledContents(True)
        self.btnAddMem.clicked.connect(self.upload)

    def ImageUpdateSlot(self, Image):
        self.loadCam.setPixmap(QPixmap.fromImage(Image))

    def CancelFeed(self):       
        self.Worker1.stop()

    def switchToCheck(self):
        checkPeople = CheckPeople()
        widget.addWidget(checkPeople)
        widget.setCurrentIndex(widget.currentIndex() + 1)
        self.Worker1.stop()

    def exitApp(self):
        exit(0)
        
class KetQua(QMainWindow):
    def __init__(self):
        super(KetQua, self).__init__()
        loadUi("GUI/ketqua2.ui", self)
        self.btnExport.clicked.connect(self.exportExcel)
        self.btnSwithToCheck.clicked.connect(self.switchToCheck)
        self.btnSwithToAdd.clicked.connect(self.switchToAdd)
        now = datetime.now()
        day = now.strftime("%d - %m - %Y")
        title = "Danh sách chấm công ngày " + day
        self.tableName.setText(title)
        self.tableWidget.setColumnWidth(0, 130)
        self.tableWidget.setColumnWidth(1, 300)
        self.tableWidget.setColumnWidth(2, 200)
        self.tableWidget.setColumnWidth(3, 200)
        self.tableWidget.setColumnWidth(4, 158)
        self.loadData()
        
    def loadData(self):
        now = datetime.now()
        day = now.strftime("%d-%m-%Y")
        file_path = day +".txt"
        file  = open(file_path, mode = "r", encoding="utf-8-sig")
        row = file.readline()
        list = []
        while (row != "" and row !="\n"):
            line = row.split(",")
            person = People(line[0], line[2], line[3])
            list.append(person)
            row = file.readline()
        self.tableWidget.setRowCount(len(list))
        i = 0
        for person in list:
            self.tableWidget.setItem(i, 0, QtWidgets.QTableWidgetItem(person.ID))
            self.tableWidget.setItem(i, 1, QtWidgets.QTableWidgetItem(person.name))
            self.tableWidget.setItem(i, 2, QtWidgets.QTableWidgetItem(person.hour))
            self.tableWidget.setItem(i, 3, QtWidgets.QTableWidgetItem(person.day))
            self.tableWidget.setItem(i, 4, QtWidgets.QTableWidgetItem(person.status))
            i = i +1
            
    def exportExcel(self):
        now = datetime.now()
        day = now.strftime("%d-%m-%Y")
        file_path = "out/" + day +".csv"
        file = open(file_path, mode= "a+")
        file = open(file_path, mode="w")
        file.write("")
        file = open(file_path, mode="r", encoding="utf-8-sig")
        line = file.readline()
        if line =="":
            file2 = open(file_path, mode="w", encoding="utf-8-sig")
            file2.write("ID,Họ và tên,Giờ,Ngày,Trạng thái\n")
        file_path2 = day + ".txt"
        file2 = open(file_path2, mode="r", encoding="utf-8-sig")
        line = file2.readline()
        while line != "" and line != "\n":
            list_2 = line.split(",")
            person = People(list_2[0], list_2[2], list_2[3])
            if person.name != "unknown":
                string = person.ID + "," + person.name + "," + person.hour +"," + person.day +"," + person.status +"\n"
                file3 = open(file_path, mode= "a+", encoding="utf-8-sig")
                file3.write(string)
            line = file2.readline()
        
    def switchToCheck(self):
        checkPeople = CheckPeople()
        widget.addWidget(checkPeople)
        widget.setCurrentIndex(widget.currentIndex() + 1)

    def switchToAdd(self):
        addPeople = AddPeople()
        widget.addWidget(addPeople)
        widget.setCurrentIndex(widget.currentIndex() + 1)

def initUi():
    file = open("dataMember.csv", mode="a+", encoding="utf-8-sig")
    file.write("")
    file = open("dataMember.csv", mode="r", encoding= "utf-8-sig")
    line = file.readline()
    while line != "":
        list = line.split(",")
        line = file.readline()

if __name__ == "__main__":
    app = QApplication(sys.argv)
    widget = QtWidgets.QStackedWidget()
    checkPeople = CheckPeople()
    initUi()
    widget.addWidget(checkPeople)
    widget.setFixedWidth(1200)
    widget.setFixedHeight(700)
    widget.show()
    try:
        sys.exit(app.exec_())
    except:
        print("Exiting")